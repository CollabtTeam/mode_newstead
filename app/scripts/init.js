$(document).ready(function () {



   $(function () {
    $('.privacy-modal-js').magnificPopup({
        type: 'inline',
        preloader: false,
            //focus: '#username',
            modal: true
        });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
});

   $(".mobile-btn").click(function (ev) {
    ev.preventDefault();
    $('.main-body-wrapper').addClass('main-wrapper-push');
    $('.mobile-menu-wrapper').addClass('visible');
    $('.mobile-btn').addClass('hidden');
    $('.close-nav').addClass('visible');
});

   $(".close-nav").click(function (ev) {
    ev.preventDefault();
    $('.main-body-wrapper').removeClass('main-wrapper-push');
    $('.mobile-menu-wrapper').removeClass('visible');
    $('.mobile-btn').removeClass('hidden');
    $('.close-nav').removeClass('visible');
});
   $('.nav-link').click(function(ev){
    ev.preventDefault();
    $(".close-nav").trigger('click');
})

   $('.carousel').each(function(){
    $(this).carousel();
})

// Add smooth scrolling to all links
$("a").on('click', function (event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            var menuHeight = $('.main-menu').height();
            $('html, body').animate({
                scrollTop: $(hash).offset().top - menuHeight
            }, 800);
        } // End if
    });

$('.js-contact-form').submit(function (ev) {

    ev.preventDefault();
    var data = $(this).serialize();

    $.ajax({
        method: 'POST',
        url: window.location.origin + window.location.pathname + 'mailer/mail.php',
        data: data,
        dataType: 'json',
        encode: true,

    }).always(function (data) {
        if (data.success) {
            $('#submit-modal').modal('show');
            $('.js-contact-form').find('input').each(function(){
             $(this).val('');
             $(this).removeAttr('checked');
         });
        }
        else {
            console.log(XMLHttpRequest, textStatus, errorThrown);
        }
    });

});

/*$('.js-popup').click(function(ev){
    ev.preventDefault();
    $('#privacy-modal').modal('show');
});*/

$('.js-scroll').mCustomScrollbar({
    theme: "dark",
    axis: "y"
});

$(window).load(function(){
    $('#loader').remove();
});

});






