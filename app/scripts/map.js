var path = window.location.origin + window.location.pathname;
var config = {
    center: {lat: -27.45337966, lng: 153.04347754},
    zoom: 16,
    icon: path + "images/m1.png",
    iconCenter: path + "images/m5.png",
    locations: [
        {
            title: 'Fortitude Valley',
            cords: {lat: -27.456500, lng: 153.0345}
        },
        {
            title: 'Rail Line',
            cords: {lat: -27.4522348, lng: 153.03675306}
        },
        {
            title: 'James St Precinct',
            cords: {lat: -27.45657616, lng: 153.03971421}
        },
        {
            title: 'Brunswick St Dining Precinct',
            cords: {lat: -27.46004151, lng: 153.03698909}
        },
        {
            title: 'Future Howard Smith Wharves',
            cords: {lat: -27.46252622, lng: 153.03725731}
        },
        {
            title: 'Teneriffe Ferry Stop',
            cords: {lat: -27.45184445, lng: 153.04944527}
        },
        {
            title: 'Mode Apartments',
            cords: {lat: -27.4521237, lng: 153.0456415}
        },
        {
            title: 'Gas Works Precinct',
            cords: {lat: -27.45055277, lng: 153.04440768}
        },
        {
            title: 'Newstead Dining Precinct',
            cords: {lat: -27.45337966, lng: 153.04347754}
        },
        {
            title: 'Tenefiffe Dining Precinct',
            cords: {lat: -27.4550172, lng: 153.04962516}
        },
        {
            title: 'Riverside Cycleway',
            cords: {lat: -27.46227156, lng: 153.05081606}
        },
        {
            title: 'Newfarm Park',
            cords: {lat: -27.46941122, lng: 153.0514276}
        },
        {
            title: 'Brisbane Powerhouse Cultural Precinct',
            cords: {lat: -27.46843074, lng: 153.0542171}
        }
    ],
    styles: [
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e9e9e9"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dedede"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#333333"
                },
                {
                    "lightness": 40
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f2f2f2"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        }
    ]
}

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: config.zoom,
        center: config.center,
        styles: config.styles

    });

    var markers = config.locations.map(function (location, i) {
        var label = null;
        var icon = null;

        if (location.title === 'Mode Apartments') {
            label = {
                text: ' '
            }
            icon = {
                url: config.iconCenter
            };
        } else {
            label = {
                text: location.title,
                color: "#0f0f0f",
                fontSize: "11px"
            };
            icon = {
                labelOrigin: new google.maps.Point(10, -10),
                url: config.icon
            };
        }

        var marker = new google.maps.Marker({
            position: location.cords,
            icon: icon,
            labelClass: 'icon-label',
            label: label,
            map: map
        });

        return marker;
    });

}

