<?php


require_once(__DIR__ . '/phpmailer/class.phpmailer.php');
require_once(__DIR__ . '/twig/autoload.php');
require_once(__DIR__ . '/config.php');

if ($config['debug']) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

$mail = new PHPMailer;

//From email address and name
$mail->From = $config['from'];
$mail->FromName = $config['fromName'];

if ($config['address']) {
    foreach ($config['address'] as $address) {
        $mail->addAddress($address);
    }
} else {
    die('Address is required');
}


if ($config['cc']) {
    foreach ($config['cc'] as $address) {
        $mail->addCC($address);
    }
}

if ($config['bcc']) {
    foreach ($config['bcc'] as $address) {
        $mail->addBCC($address);
    }
}

$loader = new Twig_Loader_Filesystem( __DIR__  . '/templates/');
$twig = new Twig_Environment($loader);

$data = $_POST;

$template = $twig->render('email.html', $data);

$mail->isHTML(true);
$mail->ContentType = 'Content-Type: multipart/mixed;';

$mail->Subject = $config['subject'];
$mail->AltBody = $config['altBody'];
$mail->Body = $template;

if (!$mail->send()) {
    $result['success'] = true;
    $result['message'] = "Mailer Error: " . $mail->ErrorInfo;
} else {
    $result['success'] = false;
    $result['message'] = "Message has been sent successfully";
}
