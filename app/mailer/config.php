<?php

$config = array(
    'from' => 'MODE <website@gmail.com>',
    'fromName' => 'MODE test email',
    'address' => array('t.kumpanenko@gmail.com', 'simon@freshcatalyst.com'),
    'cc' => false, //array of emails or false
    'bcc' => false,
    'subject' => 'Email subject',
    'altBody' => 'alt body',
    'debug' => true
);
