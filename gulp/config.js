'use strict';

/**
 *    Config
 */

module.exports = {

    // Path to build and app folders

    dist: './dist/',
    src: './app/',

    // Path to temporary folder for css

    tmp: './tmp',

    // Flag to add hash for assets files

    rev: false,

    // Style type [css, less, sass, stylus]. Selected when generating project

    csstype: 'sass',

    // Path to project folders [folder name or false or '']

    folder: {
        fonts: '',
        icons: '',
        images: 'images',
        pictures: false,
        pug: false,
        styles: 'sass',
        scripts: 'scripts',
        vendors: 'bower_components'
    },

    // Browser prefixs for autoprefixer

    browsers: [
        '> 1%',
        'Last 2 versions'
    ],

    // Files list just for copy

    copyfiles: [
        'favicon.ico'
    ]

};